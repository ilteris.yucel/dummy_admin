from ssh_service.service import SSHService

ss = SSHService()
print("Dummy SSH service")
print("#" * 30)
status = True
while status:
    con_stat = input("(1) to connect with pass (2) to connect with key  (3) exec command (4) disconnect >")
    if con_stat == '1':
        host = input("Enter hostname > ")
        user = input("Enter username > ")
        password = input("Enter password >")
        ss.connect_with_user_password(host, user, password)
        if ss.is_connected():
            print("Connection Established")
        else:
            print("Connection cannot be Established")
            status = False
    if con_stat == '2':
        host = input("Enter hostname > ")
        user = input("Enter username > ")
        keyfile = input("Enter keyfile >")
        ss.connect_with_key_file(host, user, keyfile)
        if ss.is_connected():
            print("Connection Established")
        else:
            print("Connection cannot be Established")
            status = False
    if con_stat == '3':
        command = input("Type command >")
        out, err = ss.execute_command(command)
        print("Out : {}".format(out.join(" ")))
        print("Error : {}".format(err.join(" ")))

    if con_stat == '4':
        ss.disconnect()
