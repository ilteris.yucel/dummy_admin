import paramiko


class SSHService:
    def __init__(self):
        self.client = paramiko.SSHClient()
        self.connectionStatus = False
        # self.host = None
        # self.user = None
        # self.password = None
        # self.private_key_file = None

    def connect_with_user_password(self, host, user, password):
        if self.connectionStatus:
            print("No multiple connection")
            return
        try:
            self.client.load_system_host_keys()
            self.client.connect(hostname=host, username=user, password=password)
            self.connectionStatus = True
        except paramiko.AuthenticationException as exception:
            print(exception)
            self.connectionStatus = False
        except paramiko.BadHostKeyException as exception:
            print(exception)
            self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            self.client.connect(hostname=host, username=user, password=password)
            self.connectionStatus = True
        except paramiko.SSHException as exception:
            print(exception)
            self.connectionStatus = False
        except BaseException as exception:
            print(exception)
            self.connectionStatus = False

    def connect_with_key_file(self, host, user, keyfile):
        if self.connectionStatus:
            print("No multiple connection")
            return
        try:
            pkey = paramiko.RSAKey.from_private_key_file(keyfile)
            self.client.load_system_host_keys()
            self.client.connect(hostname=host, username=user, pkey=pkey)
            self.connectionStatus = True
        except paramiko.AuthenticationException as exception:
            print(exception)
            self.connectionStatus = False
        except paramiko.BadHostKeyException as exception:
            print(exception)
            self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            self.client.connect(hostname=host, username=user, pkey=pkey)
            self.connectionStatus = True
        except paramiko.SSHException as exception:
            print(exception)
            self.connectionStatus = False
        except BaseException as exception:
            print(exception)
            self.connectionStatus = False

    def disconnect(self):
        if not self.connectionStatus:
            print("There is no connection")
            return
        self.client.close()
        self.connectionStatus = False

    def execute_command(self, command):
        if not self.connectionStatus:
            print("No connection")
            return
        _stdin, _stdout, _stderr = self.client.exec_command(command)
        out_lines = _stdout.read().decode().split("\n")
        err_lines = _stderr.read().decode().split("\n")
        return out_lines, err_lines

    def is_connected(self):
        return self.connectionStatus
