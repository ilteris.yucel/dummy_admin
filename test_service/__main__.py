from test_service.service import test_ssh_service, test_ssh_service_error

try:
    service_0 = test_ssh_service()
    service_1 = test_ssh_service_error()
except BaseException as exception:
    print(exception)
finally:
    print("Established Service Status : {}".format(service_0))
    print("Not Established Service Status : {}".format(service_1))
