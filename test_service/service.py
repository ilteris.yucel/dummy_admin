from ssh_service.service import SSHService


def test_ssh_service():
    service = SSHService()
    service.connect_with_user_password("192.168.56.20", "services", "18031994")
    return service.is_connected()


def test_ssh_service_error():
    service = SSHService()
    service.connect_with_user_password("192.168.56.20", "dummy", "dummy")
    return not service.is_connected()


