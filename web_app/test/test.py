from flask import Blueprint, jsonify, make_response
from test_service.service import test_ssh_service, test_ssh_service_error

test_bp = Blueprint("test_bp", __name__,
                    template_folder="templates",
                    static_folder="static")


@test_bp.route("/ssh-service", methods=['GET'])
def ssh_service():
    status_0 = test_ssh_service()
    status_1 = test_ssh_service_error()
    res = {
        "status": "success",
        "data": {
            "ssh connection is success": status_0,
            "ssh connection is not success": status_1
        }
    }
    return make_response(jsonify(res), 200)

