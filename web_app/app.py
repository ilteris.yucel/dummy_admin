from flask import Flask
from web_app.test.test import test_bp
from web_app.ssh.ssh import ssh_bp

app = Flask(__name__)
app.register_blueprint(test_bp, url_prefix="/tests")
app.register_blueprint(ssh_bp, url_prefix="/ssh")


@app.route("/", methods=['GET'])
def index():
    return "Success"


if __name__ == "__main__":
    app.run(debug=True)
