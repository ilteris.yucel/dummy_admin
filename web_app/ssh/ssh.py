from flask import Blueprint, make_response, jsonify, json, request
from ssh_service.service import SSHService

ssh_bp = Blueprint("ssh_bp", __name__,
                   template_folder="templates",
                   static_folder="static")

service = SSHService()


@ssh_bp.route("/connect", methods=["POST", "GET"])
def connect_ssh_service():
    data = json.loads(request.data)
    if "host" not in data or "user" not in data or "password" not in data:
        return make_response(jsonify({
            "status": "fail",
            "data": "host, user and password cannot be empty"
        }), 400)

    host = data["host"]
    user = data["user"]
    password = data["password"]

    service.connect_with_user_password(host, user, password)
    connection_status = service.is_connected()

    if connection_status:
        return make_response(jsonify({
            "status": "success",
            "data": "connection established"
        }), 200)
    return make_response(jsonify({
        "status": "fail",
        "data": "connection is not established"
    }), 400)


@ssh_bp.route("/disconnect", methods=["GET"])
def disconnect_ssh_service():
    service.disconnect()
    return make_response(jsonify({
        "status": "success",
        "data": "connection is closed"
    }), 200)
